package no.noroff.advanceddeployclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvancedDeployClassApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdvancedDeployClassApplication.class, args);
    }

}
