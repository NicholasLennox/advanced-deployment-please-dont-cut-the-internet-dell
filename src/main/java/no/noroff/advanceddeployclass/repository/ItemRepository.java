package no.noroff.advanceddeployclass.repository;

import no.noroff.advanceddeployclass.models.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {
}
